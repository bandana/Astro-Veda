<?php

class Horoscope extends BaseModel {

	/* Table Name */

	static $table_name = 'horoscope';

	static $belongs_to = array(
		
		array(
            'zodiac',
            'class_name' => 'Zodiac',
            'foreign_key' => 'zodiac_id'
        ),
	);

	/* Public functions - Setters */

	public function set_horoscope($horoscope) {

    	if($horoscope == '') {
			throw new Exception('Please Enter Horoscope for the selected date.');
        }

		$this->assign_attribute('horoscope', $horoscope);	
    }

    public function set_date($date) {

    	if($date == '') {
			throw new Exception('Please Select a Date.');
        }

		$this->assign_attribute('date', $date);	
    }

    public function set_is_pushed($is_pushed) {

    	if($is_pushed == '') {
			throw new Exception('Please Mention if the horoscope is pushed to the users.');
        }

		$this->assign_attribute('is_pushed', $is_pushed);	
    }

    public function set_zodiac($zodiac) {
    	if($zodiac == null)
    		throw new Exception("Please select a zodiac sign");    		

    	if($zodiac instanceOf Zodiac)
    		$this->assign_attribute('zodiac_id', $zodiac->id);
    }

	/* Public functions - Getters */

	public function get_horoscope() {
		return $this->read_attribute('horoscope');
	}

	public function get_date() {
		return $this->read_attribute('date');
	}

	public function get_is_pushed() {
		return $this->read_attribute('is_pushed');
	}

	/* Public static functions */

	public static function create($params) {

		$horoscope = new Horoscope;
		
		$horoscope->horoscope = array_key_exists('horoscope', $params) ? $params['horoscope'] : null;
		$user->date = array_key_exists('date', $params) ? $params['date'] : null;
		$user->zodiac = array_key_exists('zodiac', $params) ? $params['zodiac'] : null;
		$user->is_pushed = array_key_exists('is_pushed', $params) ? $params['is_pushed'] : 0;

		return $horoscope;
	}

	/* Public general functions */

	public function mark_is_pushed($horoscope_id) {

		$horoscope = Horoscope::find_by_id($horoscope_id);

		if(!$horoscope)
			throw new Exception("Inavild Horoscope");

		$horoscope->is_pushed = 1;
		$horoscope->save();
	}
}