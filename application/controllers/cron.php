<?php

class Cron extends CI_Controller
{
	public function __construct() { 
		
		parent::__construct();

        echo "testing cron";
	}

	public function index()
	{
		try {
			log_message('debug', 'Some variable was correctly set');

            $user = User::find_by_id(142);

            if(!$user) {
                throw new Exception("Invalid User!");                
            }

            $message = 'Testing cron job';

            $gcm_users = $user->gcm_users;

            $data = array(
                        'msg' => $message,
                        'msg_date' => date('Y-m-d H:i:s'),
                        );

            $message = json_encode(array(
                            'type' => 11,
                            'data' => $data
                            ));

            $this->gcm->setMessage($message);

            foreach ($gcm_users as $gcm_user) {
                $this->gcm->addRecepient($gcm_user->gcm_regd_id);
            }

            // set additional data
            $this->gcm->setData(array(
                'stat' => 'OK'
            ));

            $this->gcm->setTtl(false);
            $this->gcm->setGroup(false);
            $this->gcm->send();

            echo "test send ";
        }

        catch(Exception $e) {

            $this->session->set_flashdata('alert_error', $e->getMessage());
            echo "Exception";
            redirect('dashboard');
        }
	}

    public function test(){
        echo "hello testing";
    }
}