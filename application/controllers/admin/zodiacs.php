<?php

class Zodiacs extends BaseController {

	public function index() {

        $horoscopes = Horoscope::find('all', array(
                                        'conditions' => array(
                                            'date >= ?
                                            and date <= ?',
                                            date("Y-m-d"),
                                            date("Y-m-d", strtotime("+7 day")),
                                            ),
                                        )
                                    );

	   return $this->load_view('admin/zodiac', array('horoscopes' => $horoscopes));
    }

    public function add() {

        try {

        	if ($_SERVER['REQUEST_METHOD'] !== 'POST') {
               //redirect('/admin/zodiac');
            }
            
            for($i = 1; $i <= 12; $i++) {
                
                $zodiac = Zodiac::find_by_id($i);

                if(!$zodiac)
                    throw new Exception("Invalid Zodiac");

                $params = array(
                                    'zodiac' => $zodiac,
                                    'horoscope' => $this->input->post('$horoscope'.$i),
                                    'date' => $this->input->post('date'),
                                );

                $new_horoscope = new Horoscope();
                $horoscope = $new_horoscope->create($params);
                $horoscope->save();
            }

            $this->session->set_flashdata('alert_success', 'Horoscope saved');
            //redirect('/admin/zodiac');
        }

        catch(Exception $e) {

            $this->session->set_flashdata('alert_error', $e->getMessage());
            //redirect('/admin/zodiac');
        }
    }

    public function edit($horoscope_id) {

    }
}