<?php

class Dashboard extends BaseController {

	public function index() {
	
    	return $this->load_view('dashboard');
    }

    public function message() {

        try {

            $users = User::find('all', array(
                                            'conditions' => array(
                                                'deleted = ?
                                                and active = ?',
                                                0,
                                                1
                                                ),
                                            'order' => 'id desc'
                                            )
                                    );

            if(!$users) {
                throw new Exception("No user found!");                
            }

            if ($_SERVER['REQUEST_METHOD'] !== 'POST') {

               redirect('dashboard');
            }

            $message = $this->input->post('message');
            if(!$message)
                throw new Exception("Please enter a message");

            foreach ($users as $user) {

                $gcm_users = $user->gcm_users;

	            $data = array(
	                        'msg' => $message,
                            'msg_date' => date('Y-m-d H:i:s'),
	                        );

	            $message_json = json_encode(array(
	                            'type' => 8,
	                            'data' => $data
	                            ));

	            $this->gcm->setMessage($message_json);

	            foreach ($gcm_users as $gcm_user) {
	                $this->gcm->addRecepient($gcm_user->gcm_regd_id);
	            }

	            // set additional data
	            $this->gcm->setData(array(
	                'stat' => 'OK'
	            ));

	            $this->gcm->setTtl(false);
	            $this->gcm->setGroup(false);
	            $this->gcm->send();
	        }

            $this->session->set_flashdata('alert_success', "Message sent to the users successfully");

            redirect('dashboard');
        }

        catch(Exception $e) {

            $this->session->set_flashdata('alert_error', $e->getMessage());

            redirect('dashboard');
        }   
    }
}

?>