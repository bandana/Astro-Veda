<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_column_queries extends CI_Migration {

	public function up() {

		$fields = array(

			'draft' => array(
				'type' => 'varchar',				
				'constraint'=>'3000',
				'null' => true,
			),
		);

		$this->dbforge->add_column('queries', $fields);
	}

	public function down() {

		$this->dbforge->drop_column('queries','draft');
	}
}