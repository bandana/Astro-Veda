<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Alter_push_notification_log extends CI_Migration {

	public function up() {

		$fields = array(
                        'details' => array(
                                         'name' => 'details',
                                         'type' => 'varchar',
                                         'constraint' => '3000',
                                         'null' => true,
                                ),
					);

		$this->dbforge->modify_column('push_notification_log', $fields);
	}

	public function down() {
		
		$fields = array(
                        'details' => array(
                                         'name' => 'details',
                                         'type' => 'varchar',
                                         'constraint' => '250',
                                ),
					);

		$this->dbforge->modify_column('push_notification_log', $fields);
	}
}