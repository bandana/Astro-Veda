<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_horoscope extends CI_Migration {

	public function up() {

		$fields = array(
		
			'id' => array(
				'type' => 'int',
				'auto_increment' => true,
			),

			'zodiac_id' => array(
				'type' => 'int',
			),

			'horoscope' => array(
				'type' => 'varchar',				
				'constraint'=>'2000',
				'null' => false,
			),

			'date' => array(
				'type' => 'date',
				'null' => False,
			),

			'is_pushed' => array(
				'type' => 'boolean',
				'default' => 0,
			),

			'created_at' => array(
				'type' => 'timestamp',
			),
			
			'modified_at' => array(
				'type' => 'timestamp',
			),
		);

		$this->dbforge->add_field($fields);
		$this->dbforge->add_key('id', true);
		$this->dbforge->create_table('horoscope');

		//$this->db->query('ALTER TABLE horoscope MODIFY modified_at TIMESTAMP ON UPDATE CURRENT_TIMESTAMP');
	}

	public function down() {
		$this->dbforge->drop_table('horoscope');
	}
}