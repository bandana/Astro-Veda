<?php extend('common/base') ?>

<?php startblock('content') ?>


    <link rel="stylesheet" type="text/css" href="<?php echo base_url('public/css/style.css');?>">

    <div class="col-md-12 col-sm-12">
      <div class="container" style="padding-top:50px;">
        <div class="row">
      <div class="query col-md-6 col-sm-6">
        <h2 id="tophead">Query History (<?php echo $user->queries_count?>)</h2>
            <div class="qform">
            <form>
              <?php for($i=1; $i<count($queries); $i++) { ?>
              <div class="form-group">
                <textarea class="qno" cols="100"  readonly><?php echo $queries[$i]->query;?></textarea>
                <textarea class="form-control" cols="100"  readonly><?php echo $queries[$i]->answer;?></textarea>
              </div>
              <?php } ?>
             </form>
            </div>
      </div>
      <div class="profile col-md-6 col-sm-6">
        <h2 id="tophead">Profile</h2>
        <?php if($user->profile_pic != '') { ?>
        <img class="pro-img img-responsive" style="height:170px;width:170px;" src="<?php echo base_url($user->profile_pic);?>">
        <?php } else { ?>
        <img class="pro-img img-responsive" style="height:170px;width:170px;" src="<?php echo base_url('public/user_images/profile.png'); ?>">
        <?php } ?>
        <ul class="profile-list">
          <li>Name: <?php echo $user->get_full_name();?></li>
          <li>DOB: <?php echo date('M d, Y', strtotime($user->date_of_birth));?></li>
          <li>TOB: <?php echo date('H:i:s', strtotime($user->date_of_birth));?></li>
          <li>POB: <?php echo $user->place_of_birth;?></li>
          <li>Gender: <?php if($user->gender == 0)
                              echo "Male";
                             else
                              echo "Female";?></li>
          <li>Zodiac Sign: <?php if($user->zodiac) echo $user->zodiac->zodiac; else { echo "Not assigned";?>&nbsp&nbsp&nbsp&nbsp<a href="<?php echo base_url('admin/users/assign_zodiac/'.$user->id);?>">Assign Zodiac Sign</a><?php } ?></li>
          <li>Puja: <?php if($pujas) {
                            foreach ($pujas as $puja) {
                              echo $puja->name. ', ';
                          }}?></li>
          <li>Gemstone: <?php if($user_gemstones) {
                            foreach ($user_gemstones as $user_gemstone) {
                              echo $user_gemstone->gemstone->name . ', ';
                          }}?></li>
          <li><button class="btn btn-primary btn-block" style="width:80%" data-toggle="modal" data-target="#myModal">Send a Message</button></li>
        </ul>

        <div class="clear"></div>
        <?php if(count($queries) > 0) { ?>
          <form class="form" role="form" method ="POST" action="<?php echo base_url('admin/queries/answer/'.$queries[0]->id);?>">
              <div class="form-group">
                <textarea class="qno" cols="100"  readonly><?php echo $queries[0]->query?></textarea>
                <textarea class="form-control" cols="100"  name="answer" placeholder="Enter Your Answer"><?php echo ($queries[0]->draft) ? $queries[0]->draft : $queries[0]->answer?></textarea>
              </div>
              <?php if(!$queries[0]->answer)  { ?>
              <div class="row">
                <div class="col-md-6">
                <div class="form-group">
                  <input type="submit" class="btn btn-lg btn-primary btn-block" name="draft" value="Save as Draft">
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <input type="submit" class="btn btn-lg btn-primary btn-block" name="final_answer" value="Answer" onclick="return confirm_answer();">
                </div>
              </div>
              </div>
              </div>
              <!-- <button type="button" class="butt btn btn-primary">Save as Draft</button>
              <button type="button" class="butt btn btn-primary">Answer</button> -->
              <?php } ?>
        </form>
        <?php } ?> 
      </div>
    </div>
  </div>
  </div>


<!-- Modal -->
  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
          <h4 class="modal-title" id="myModalLabel">Send a Message</h4>
        </div>

        <form class="form" role="form" method ="POST" action="<?php echo base_url('admin/users/message/'.$user->id);?>">
        <div class="modal-body">
          <div class="form-group" style="width:80%;">

              <label for="message">Message</label>
              <textarea class="form-control" rows="5" name="message"  id="message" placeholder="Enter message here" required></textarea>
          </div>

        </div>
        <div class="modal-footer">
     
          <button type="button" id="close_modal" class="btn btn-default" data-dismiss="modal">Close</button>
          <button type="submit" class="btn btn-primary" onclick="return confirm_message();">Send</button>
        </div>
      </form> 
  </div>
</div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript">

      function confirm_answer() {
        return confirm('Are you sure to send this answer to the user?');
      }

      function confirm_message() {
        return confirm('Are you sure to send this message to the user?');
      }

    </script>

<?php endblock() ?>

<?php end_extend() ?>