<?php extend('common/base') ?>

<?php startblock('content') ?>

<div class="container">
	<div class="row-fluid">
	<div class="col-md-12">
		
		Welcome		

	</div>

	<div class="col-md-6" style="padding-top:50px;">
	<form class="form" role="form" method ="POST" action="<?php echo base_url('dashboard/message');?>">
      <div class="form-group">
      	<label for="message">Message</label>
      	<textarea class="form-control" rows="5" name="message"  id="message" placeholder="Enter message here to send to all users" required></textarea>
      </div>
      
       <div class="form-group">
          <input type="submit" style="width:50%;align:centre" class="btn btn-primary btn-block" name="send" value="Send Message" onclick="return confirm_message();">
       </div>
    </form>
</div>
</div>

</div>

<?php endblock() ?>

<?php end_extend() ?>

<script type="text/javascript">

  function confirm_message() {
    return confirm('Are you sure to send this message to the all the users?');
  }

</script>